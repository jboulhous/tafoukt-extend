(function (Meteor, Tinytest) {

    Tinytest.add('extend - function attached to Meteor', function (test) {
        test.isTrue(_.isFunction(Meteor.extend));
    });
    
    Tinytest.add('extend - test extend', function (test) {
        var Car = function(model){
            this.model = model;
        };
        
        Car.extend = Meteor.extend ;
        
        var Bus = Car.extend({
            type : "bus"
        });
        
        var bus = new Bus("mercedes");
        test.equal(bus.model,"mercedes");
        test.equal(bus.type, "bus");
        
    });


}(Meteor, Tinytest));
