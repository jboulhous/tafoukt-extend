var path = require('path');

Package.describe({
  summary: "Backbone.Model.extend"
});


Package.on_use(function (api, where) {
  where = where || ['client', 'server'];

  api.use(['underscore'], where);


  api.add_files('extend.js', where);
});

Package.on_test(function (api) {
  api.use('tinytest');
  api.use('extend', ['client', 'server']);
  api.add_files('extend.tests.js', ['client', 'server']);
});
